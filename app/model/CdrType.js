/**
 * Created by Samira on 8/13/17.
 */
Ext.define('cdrReport.model.CdrTypeModel', {
    extend: 'Ext.data.Model',
    fields: ['cdrType', 'count']
});