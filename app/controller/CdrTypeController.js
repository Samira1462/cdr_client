/**
 * Created by Samira on 8/13/17.
 */
Ext.define('cdrReport.controller.CdrTypeController',{
    extend: 'Ext.app.Controller',

    refs: [{
        ref: 'cdrtype',
        selector: 'cdrtype'
    }],

    stores: ['CdrTypeStore'],

    init: function() {
        // Start listening for events on views
        this.control({
          /*  'stationslist': {
                selectionchange: this.onStationSelect
            }*/
        });
    }

});

