/**
 * Created by Samira on 8/13/17.
 */
Ext.define('cdrReport.store.CdrTypeStore', {
    extend: 'Ext.data.Store',
    alias: 'store.cdrtypes',
    requires: 'cdrReport.model.CdrTypeModel',
    model: 'cdrReport.model.CdrTypeModel',

    storeId:'cdrTypeStore',

    proxy: {
        type: 'ajax',
        url: 'data/stations.json',
        reader: {
            type: 'json',
            root: 'results'
        }
    }

/*    fields:['count', 'type'],
    data:{'items':[
        { 'name': 'Lisa',  "email":"lisa@simpsons.com"},
        { 'name': 'Bart',  "email":"bart@simpsons.com"},
        { 'name': 'Homer', "email":"homer@simpsons.com"},
        { 'name': 'Marge', "email":"marge@simpsons.com"}
    ]},
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            root: 'items'
        }
    }*/
});